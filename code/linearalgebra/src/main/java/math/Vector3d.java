// Bianca Rossetti
package math;
/**
 * Vector is a class used for creating Vector types in java
 * and storing information about these vectors.
 * @author Bianca Rossetti
 * @version 10/02/2023
 */
public class Vector3d {
    private double x;
    private double y;
    private double z;

    /**
     * Construct the Vector object pased on input parameters
     * @param x The value of the x field
     * @param y the value of the y field
     * @param z the value of the z field
     * @return Vector object
     */
    public Vector3d(double x, double y, double z)
    {
        this.x = x;
        this.y = y;
        this.z = z;
    }


    /**
     * Used to access the value of x field
     * @return The value of x
     */
    public double getX()
    {
        return(this.x);
    }

    /**
     * Used to access the value of y field
     * @return The value of y
     */
    public double getY()
    {
        return(this.y);
    }

    /**
     * Used to access the value of z field
     * @return The value of z
     */
    public double getZ()
    {
        return(this.z);
    }

    /**
     * Used to calculate the magnitude
     * @return The magnitude
     */
    public double magnitude()
    {
        return(Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2)));
    }

    /**
     * Calculates the dot product of the vector object
     * @return The value of the dot product
     */
    public double dotProduct(Vector3d vector)
    {
        return((this.x * vector.getX()) + (this.y * vector.getY()) + (this.z * vector.getZ()));
    }

    /**
     * Calculates the sum of 2 vectors
     * @return The new Vector  summed up
     */
    public Vector3d add(Vector3d vector)
    {
        double newX = this.x + vector.getX();
        double newY = this.y + vector.getY();
        double newZ = this.z + vector.getZ();

        Vector3d addedVector = new Vector3d(newX, newY, newZ);
        return(addedVector);
    }
}
